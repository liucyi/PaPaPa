关于 PaPaPa
-------------
> PaPaPa 是 [C#爱好者](http://shang.qq.com/wpa/qunwpa?idkey=bcb7dc5f9da01d0c78d5b037f7d3101a3a79b3a246fd8e9ec6f9d580e61f8fdf) 利用业余时间，以技术分享与学习为目的完成的非商业项目。

> 目前使用到的技术包括：

  * ASP.Net MVC5
  * EF6
  * Bootstrap 3
  * SignalR
  * Redis

PaPaPa 的功能
-------------
> 详见需求文档 Docs 文件夹下的word、excel、ppt

意见和建议
-------------
> 如果您有意见和建议，欢迎您可以加入我们的群：313995187，一起学习，一起成长。