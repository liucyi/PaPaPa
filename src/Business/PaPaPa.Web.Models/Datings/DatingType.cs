﻿using Framework.Common.EnumOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Models.Datings
{
    /// <summary>
    /// 约会类型
    /// </summary>
    public enum DatingType
    {
        /// <summary>
        /// 吃饭
        /// </summary>
        [Extension("吃饭")]
        Eat = 1,
        /// <summary>
        /// 看电影
        /// </summary>
        [Extension("看电影")]
        Movie = 2,
        /// <summary>
        /// 血拼
        /// </summary>
        [Extension("血拼")]
        Shopping = 3,
        /// <summary>
        /// 旅行
        /// </summary>
        [Extension("旅行")]
        Trip = 4,
    }
}
