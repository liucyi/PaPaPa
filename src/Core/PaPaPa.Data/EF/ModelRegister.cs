﻿using PaPaPa.Models.Accounts;
using PaPaPa.Models.Datings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Data.EF
{
    public class ModelRegister
    {
        internal static void Regist(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("dbo.User");
            modelBuilder.Entity<DatingInfo>().ToTable("dbo.DatingInfo");
            modelBuilder.Entity<DataApplication>().ToTable("dbp.DataApplication");
        }
    }
}
