﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaPaPa
{
    public partial class Startup
    {
        public void ConfigureChat(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}