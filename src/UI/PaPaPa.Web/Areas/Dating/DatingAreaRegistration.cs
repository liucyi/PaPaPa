﻿using System.Web.Mvc;

namespace PaPaPa.Areas.Dating
{
    public class DatingAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Dating";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Dating_default",
                "Dating/{controller}/{action}/{id}",
                new { action = "Create", id = UrlParameter.Optional }
            );
        }
    }
}