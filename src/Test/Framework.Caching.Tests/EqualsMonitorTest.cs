﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Caching.Monitors;

namespace Framework.Caching.Tests
{
    [TestClass]
    public class EqualsMonitorTest
    {
        public class TestMonitor : IEquatable<TestMonitor>
        {
            public string TestString { get; set; }

            #region IEquatable<TestMonitor> 成员

            public bool Equals(TestMonitor other)
            {
                if (other == null)
                {
                    return false;
                }

                return this.TestString == other.TestString;
            }

            #endregion
        }

        public class TestMonitor2 : IEquatable<TestMonitor2>
        {
            public string TestString { get; set; }

            #region IEquatable<TestMonitor> 成员

            public bool Equals(TestMonitor2 other)
            {
                if (other == null)
                {
                    return false;
                }

                return this.TestString == other.TestString;
            }

            #endregion
        }

        [TestMethod]
        public void TestStringEqualsMonitor()
        {
            string key = "test";
            string key2 = "test2";
            var monitor = new TestMonitor() { TestString = "monitor" };
            var monitor2 = new TestMonitor2() { TestString = "monitor" };

            EqualsMonitorManager<string, TestMonitor>.Add(key, monitor);

            Assert.IsTrue(EqualsMonitorManager<string, TestMonitor>.IsMonitoring(key, monitor));

            Assert.IsFalse(EqualsMonitorManager<string, TestMonitor>.IsMonitoring(key2, monitor));
            Assert.IsFalse(EqualsMonitorManager<string, TestMonitor2>.IsMonitoring(key, monitor2));

            EqualsMonitorManager<string, TestMonitor>.Remove(key);

            Assert.IsFalse(EqualsMonitorManager<string, TestMonitor>.IsMonitoring(key, monitor));
        }

        [TestMethod]
        public void TestIntEqualsMonitor()
        {
            try
            {
                var monitor = new TestMonitor() { TestString = "monitor" };
                EqualsMonitorManager<int, TestMonitor>.Add(5, monitor);
                Assert.Fail("逆泛型失败");
            }
            catch (NullReferenceException) { }
        }
    }
}
